/* See LICENSE file for copyright and license details. */
/* appearance */
static unsigned int borderpx  = 1;              /* border pixel of windows */
static unsigned int snap      = 32;             /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int gappx          = 6;   /* gaps between windows */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static int showbar                  = 1;        /* 0 means no bar */
static int topbar                   = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Monospace:size=10" };
static char normbgcolor[]           = "#24283b";
static char selbgcolor[]            = "#414868";
static char normfgcolor[]           = "#c0caf5";
static char selfgcolor[]            = "#c0caf5";
static char normbordercolor[]       = "#a9b1d6";
static char selbordercolor[]        = "#7aa2f7";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

/* tagging */
/* static const char *tags[] = { "", "", "", "", "ﭮ", "6", "7", "8", "9" }; */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
    /* xprop(1):
     *  WM_CLASS(STRING) = instance, class
     *  WM_NAME(STRING) = title
     */
    /* class         instance    title       tags mask     isfloating   monitor */
    /* { "firefox",     "Picture-In-Picture", NULL, 0 << 0,   1,           -1 }, */
    { "firefox",     NULL, "Picture-In-Picture", 0 << 0,   1,           -1 },
};

/* layout(s) */
static float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
    /* symbol     arrange function */
    { "[]=",      tile },    /* first entry is default */
    { "><>",      NULL },    /* no layout function means floating behavior */
    { "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
        { "normbgcolor",        STRING,  &normbgcolor },
        { "normbordercolor",    STRING,  &normbordercolor },
        { "normfgcolor",        STRING,  &normfgcolor },
        { "selbgcolor",         STRING,  &selbgcolor },
        { "selbordercolor",     STRING,  &selbordercolor },
        { "selfgcolor",         STRING,  &selfgcolor },
        { "borderpx",           INTEGER, &borderpx },
        { "snap",               INTEGER, &snap },
        { "showbar",            INTEGER, &showbar },
        { "topbar",             INTEGER, &topbar },
        { "nmaster",            INTEGER, &nmaster },
        { "resizehints",        INTEGER, &resizehints },
        { "mfact",              FLOAT,   &mfact },
};

/* Needed for XF86 keys */
#include <X11/XF86keysym.h>
/* Keybindings */
static Key keys[] = {
    /* modifier                     key        function        argument */
    { MODKEY|ShiftMask,             XK_b,      togglebar,      {0} },
    { MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
    { MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
    { MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
    { MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
    { MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
    { MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
    { MODKEY,                       XK_Return, zoom,           {0} },
    { MODKEY,                       XK_Tab,    view,           {0} },
    { MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
    /* { MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} }, */
    /* { MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} }, */
    /* { MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} }, */
    /* { MODKEY,                       XK_space,  setlayout,      {0} }, */
    { MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
    { MODKEY,                       XK_f,      togglefullscr,  {0} },
    { MODKEY,                       XK_0,      view,           {.ui = ~0 } },
    { MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
    { MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
    { MODKEY,                       XK_period, focusmon,       {.i = +1 } },
    { MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
    { MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
    TAGKEYS(                        XK_1,                      0)
    TAGKEYS(                        XK_q,                      0)
    TAGKEYS(                        XK_2,                      1)
    TAGKEYS(                        XK_w,                      1)
    TAGKEYS(                        XK_3,                      2)
    TAGKEYS(                        XK_e,                      2)
    TAGKEYS(                        XK_4,                      3)
    TAGKEYS(                        XK_5,                      4)
    TAGKEYS(                        XK_backslash,              4)
    TAGKEYS(                        XK_6,                      5)
    TAGKEYS(                        XK_7,                      6)
    TAGKEYS(                        XK_8,                      7)
    TAGKEYS(                        XK_bracketright,           7)
    TAGKEYS(                        XK_9,                      8)
    TAGKEYS(                        XK_bracketleft,            8)
    { MODKEY|ALTKEY|ShiftMask,      XK_q,      quit,           {0} },
    /* Not dwm specific */
    { MODKEY,                       XK_space,  spawn,          SHCMD("j4-dmenu-desktop --dmenu=${GMENU} --no-generic") },
    { MODKEY|ALTKEY,                XK_space,  spawn,          SHCMD("gmenu-run-i") },
    { MODKEY|ShiftMask,             XK_m,      spawn,          SHCMD("mmclaunch") },
    { MODKEY,                       XK_t,      spawn,          SHCMD("$TERMINAL -e tmux-wrapper") },
    { MODKEY,                       XK_b,      spawn,          SHCMD("$BROWSER") },
    { MODKEY,                       XK_c,      spawn,          SHCMD("clipmenu") },
    { MODKEY,                       XK_p,      spawn,          SHCMD("passmenu-lite") },
    { ALTKEY,                       XK_space,  spawn,          SHCMD("dunstctl close") },
    { 0,             XF86XK_AudioRaiseVolume,  spawn,          SHCMD("pamixer --increase 5  ; pkill -RTMIN+10 dwmblocks") },
    { 0,             XF86XK_AudioLowerVolume,  spawn,          SHCMD("pamixer --decrease 5  ; pkill -RTMIN+10 dwmblocks") },
    { 0,                    XF86XK_AudioMute,  spawn,          SHCMD("pamixer --toggle-mute ; pkill -RTMIN+10 dwmblocks") },
    { 0,                    XF86XK_AudioNext,  spawn,          SHCMD("playerctl next --player spotify || playerctl next") },
    { 0,                    XF86XK_AudioPrev,  spawn,          SHCMD("playerctl previous --player spotify || playerctl previous") },
    { 0,                    XF86XK_AudioPlay,  spawn,          SHCMD("playerctl play-pause --player spotify || playerctl play-pause") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function        argument */
    { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
    { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
    { ClkWinTitle,          0,              Button2,        zoom,           {0} },
    { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
    { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
    { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
    { ClkTagBar,            0,              Button1,        view,           {0} },
    { ClkTagBar,            0,              Button3,        toggleview,     {0} },
    { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
