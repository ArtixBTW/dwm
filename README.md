# Switched to qtile
# My build of dwm
## Patches
- Actual Fullscreen ( Add fullscreen to dwm )
- AlwaysCenter      ( Center floating windows )
- Steam             ( Fix some Steam window bugs? )
- Systray           ( Add system tray to dwm )
- UselessGap        ( Add 6px gap inbetween windows )
- Xresources        ( Add Xresources support )
- Tokyo Night       ( Colorscheme )

## Help/FAQ

- What version of dwm is this?
This build is currently on version 6.2
